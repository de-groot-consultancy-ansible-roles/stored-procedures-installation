
CREATE DATABASE IF NOT EXISTS _test_child_schema;
CREATE DATABASE IF NOT EXISTS _test_parent_schema;

SET foreign_key_checks=0;
DROP TABLE IF EXISTS `_test_child_schema`.`child`;
DROP TABLE IF EXISTS `_test_child_schema`.`child_old`;
DROP TABLE IF EXISTS `_test_parent_schema`.`parent`;
DROP TABLE IF EXISTS `_test_parent_schema`.`parent_old`;
SET foreign_key_checks=1;

CREATE TABLE `_test_child_schema`.`child` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` bigint(20) unsigned NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `parentid` (`parentid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

CREATE TABLE `_test_child_schema`.`child_old` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` bigint(20) unsigned NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `parentid` (`parentid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;


CREATE TABLE `_test_parent_schema`.`parent_old` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  UNIQUE KEY `id` (`id`),
  CONSTRAINT `parent_old_to_child` FOREIGN KEY (`id`) REFERENCES `_test_child_schema`.`child` (`parentid`),
  CONSTRAINT `parent_old_to_child_old` FOREIGN KEY (`id`) REFERENCES `_test_child_schema`.`child_old` (`parentid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `_test_parent_schema`.`parent` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  UNIQUE KEY `id` (`id`),
  CONSTRAINT `parent_to_child` FOREIGN KEY (`id`) REFERENCES `_test_child_schema`.`child` (`parentid`),
  CONSTRAINT `parent_to_child_old` FOREIGN KEY (`id`) REFERENCES `_test_child_schema`.`child_old` (`parentid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;


ALTER TABLE _test_child_schema.child ADD   CONSTRAINT `child_to_parent_old_1` FOREIGN KEY (`parentid`) REFERENCES `_test_parent_schema`.`parent_old` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE _test_child_schema.child ADD   CONSTRAINT `child_to_parent_old_2` FOREIGN KEY (`parentid`) REFERENCES `_test_parent_schema`.`parent_old` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE _test_child_schema.child ADD   CONSTRAINT `child_to_parent_old_3` FOREIGN KEY (`parentid`) REFERENCES `_test_parent_schema`.`parent_old` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE _test_child_schema.child ADD   CONSTRAINT `child_to_parent_1` FOREIGN KEY (`parentid`) REFERENCES `_test_parent_schema`.`parent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE _test_child_schema.child ADD   CONSTRAINT `child_to_parent_2` FOREIGN KEY (`parentid`) REFERENCES `_test_parent_schema`.`parent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE _test_child_schema.child ADD   CONSTRAINT `child_to_parent_3` FOREIGN KEY (`parentid`) REFERENCES `_test_parent_schema`.`parent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE _test_child_schema.child_old ADD   CONSTRAINT `child_old_to_parent_old_1` FOREIGN KEY (`parentid`) REFERENCES `_test_parent_schema`.`parent_old` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE _test_child_schema.child_old ADD   CONSTRAINT `child_old_to_parent_old_2` FOREIGN KEY (`parentid`) REFERENCES `_test_parent_schema`.`parent_old` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE _test_child_schema.child_old ADD   CONSTRAINT `child_old_to_parent_old_3` FOREIGN KEY (`parentid`) REFERENCES `_test_parent_schema`.`parent_old` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE _test_child_schema.child_old ADD   CONSTRAINT `child_old_to_parent_1` FOREIGN KEY (`parentid`) REFERENCES `_test_parent_schema`.`parent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE _test_child_schema.child_old ADD   CONSTRAINT `child_old_to_parent_2` FOREIGN KEY (`parentid`) REFERENCES `_test_parent_schema`.`parent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE _test_child_schema.child_old ADD   CONSTRAINT `child_old_to_parent_3` FOREIGN KEY (`parentid`) REFERENCES `_test_parent_schema`.`parent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

# This test should not have effect because the tables were creates less then an hour ago.
SELECT "Running drop_table_wildcard in EXECUTE mode for _old tables created over an hour ago: " AS test_status;
CALL drop_table_wildcard('\_test\_%', '%_old', 3600, 1);
SELECT "Four tables should be remaining" AS test_status, COUNT(*) AS tables_remaining FROM information_schema.tables where table_schema IN ('_test_parent_schema', '_test_child_schema');

# Sleeping 2 seconds so we can run tests with matching tables
SELECT "Slept 2 seconds to allow execution for tables creates over 1 second ago" AS test_status, SLEEP(2) AS sleep_result;

# This test should not have effect because in CHECK mode
SELECT "Running drop_table_wildcard for _old tables creating over a second ago in CHECK mode: " AS test_status;
CALL drop_table_wildcard('\_test\_%', '%_old', 1, 0);

SELECT "Four tables should be remaining" AS test_status, COUNT(*) AS tables_remaining FROM information_schema.tables where table_schema IN ('_test_parent_schema', '_test_child_schema');

# This test should have effect because in EXECUTE mode, dropping 2 tables matching %_old
SELECT "Running drop_table_wildcard for _old tables creating over a second ago in EXECUTE mode: " AS test_status;
CALL drop_table_wildcard('\_test\_%', '%_old', 1, 1);

SELECT "Two tables should be remaining" AS test_status, COUNT(*) AS tables_remaining FROM information_schema.tables where table_schema IN ('_test_parent_schema', '_test_child_schema');

# This test should not have effect because in CHECK mode
SELECT "Running CHECK for drop_table_with_referencing_constraints" AS test_status;
CALL drop_table_with_referencing_constraints('_test_parent_schema', 'parent', 0);

SELECT "Two tables should be remaining" AS test_status, COUNT(*) AS tables_remaining FROM information_schema.tables where table_schema IN ('_test_parent_schema', '_test_child_schema');

# This test should have effect bcause in EXECUTE mode
SELECT "Running EXECUTE for drop_table_with_referencing_constraints" AS test_status;
CALL drop_table_with_referencing_constraints('_test_parent_schema', 'parent', 1);

SELECT "One table should be remaining" AS test_status, COUNT(*) AS tables_remaining FROM information_schema.tables where table_schema IN ('_test_parent_schema', '_test_child_schema');

# Dropping remains
SET foreign_key_checks=0;
DROP DATABASE IF EXISTS _test_parent_schema;
DROP DATABASE IF EXISTS _test_child_schema;
SET foreign_key_checks=1;

SELECT "No tables should be remaining, databases were cleaned up" AS test_status, COUNT(*) AS tables_remaining FROM information_schema.tables where table_schema IN ('_test_parent_schema', '_test_child_schema');
