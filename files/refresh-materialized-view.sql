DELIMITER ;

DROP PROCEDURE IF EXISTS refresh_materialized_view;

DELIMITER //

CREATE PROCEDURE refresh_materialized_view
(IN view_schema VARCHAR(255), IN view_name VARCHAR(255))
SQL SECURITY INVOKER
BEGIN

        SELECT CONCAT("Materializing view `", view_schema, "`.`", view_name, "` into `", view_schema, "`.`", view_name, "_materialized`") AS message;

        SET @drop_statement=CONCAT("DROP TABLE IF EXISTS `", view_schema, "`.`", view_name, "_materialized_new`;");
        PREPARE drop_statement FROM @drop_statement;
        EXECUTE drop_statement;
        SELECT "Deleted potential garbage from old run" AS message;

        SET @initialrun_statement=CONCAT("CREATE TABLE IF NOT EXISTS `", view_schema, "`.`", view_name, "_materialized` AS SELECT * FROM `", view_schema, "`.`", view_name, "` LIMIT 1");
        PREPARE initialrun_statement FROM @initialrun_statement;
        EXECUTE initialrun_statement;
        SELECT "Created, if needed, the empty initial run materialized view" AS message;

        SET @schema_statement=CONCAT("CREATE TABLE `", view_schema, "`.`", view_name, "_materialized_new` LIKE `", view_schema, "`.`", view_name, "_materialized`");
        PREPARE schema_statement FROM @schema_statement;
        EXECUTE schema_statement;
        SELECT "Created new materialized view like the production version to save any indexes that were created" AS message;

        SET @insert_statement=CONCAT("INSERT INTO `", view_schema, "`.`", view_name, "_materialized_new` SELECT * FROM `", view_schema, "`.`", view_name, "`;");
        PREPARE insert_statement FROM @insert_statement;
        EXECUTE insert_statement;
        SELECT "Refreshed the materialized view into _new table" AS message;

        SET @swap_statement=CONCAT("RENAME TABLE `", view_schema, "`.`", view_name, "_materialized` TO `", view_schema, "`.`", view_name, "_materialized_old`, `", view_schema, "`.`", view_name, "_materialized_new` TO `", view_schema, "`.`", view_name, "_materialized`;");
        PREPARE swap_statement FROM @swap_statement;
        EXECUTE swap_statement;
        SELECT "Took the new materialized view to production" AS message;

        SET @drop_statement=CONCAT("DROP TABLE IF EXISTS `", view_schema, "`.`", view_name, "_materialized_old`;");
        PREPARE drop_statement FROM @drop_statement;
        EXECUTE drop_statement;
        SELECT "Collected garbage" AS message;
END 
//

DELIMITER ;
