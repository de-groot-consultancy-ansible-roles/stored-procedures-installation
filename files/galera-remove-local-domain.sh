#!/bin/bash

# This script is a work-around until https://jira.mariadb.org/browse/MDEV-34487 is implemented.

gtid_domain_id=$(mysql -Be "SHOW GLOBAL VARIABLES LIKE 'gtid_domain_id'"|grep gtid_domain_id |awk '{print $2}')

cd $(dirname $0)

has_local_domain=$(mysql -Be "SHOW GLOBAL VARIABLES LIKE 'gtid_binlog_pos'"|grep gtid_binlog_pos|awk '{ print $2 }'|tr ',' '\n'|grep $gtid_domain_id)

if [ -n "$has_local_domain" ]; then
	echo "Local domain ID found, proceeding to delete it."
	./remove-mariadb-gtid-domain.sh $gtid_domain_id
else
	echo "Local gtid_domain_id $gtid_domain_id not found in gtid_binlog_pos."
fi
