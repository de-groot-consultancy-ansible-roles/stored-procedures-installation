#!/bin/bash

if [ $# -lt 1 ]; then
	echo "Usage: $0 DOMAIN"
	echo "For example: $0 0" to remove the default GTID domain id 0
fi

domain="$1"

echo "Attempting to remove $domain from the running instance"
mysql -e "FLUSH BINARY LOGS DELETE_DOMAIN_ID=($domain)" >/dev/null 2>/dev/null

if [ $? -eq 0 ]; then
	echo "Success! The domain is removed."
	exit 1
fi

echo "Failed. Flushing the binary log, then purging binary logs until the domain has dissapeared."
mysql -e "FLUSH BINARY LOGS"

next_binary_log=$(mysql -Be "SHOW BINARY LOGS"|grep -v "Log_name"|head -n 2|tail -n 1|awk '{print $1}')
while [ -n "$next_binary_log" ]
do
	echo "Purging oldest binary logs before $next_binary_log"
	mysql -e "PURGE BINARY LOGS TO '$next_binary_log'"

	echo "Attempting to remove the domain again"
	mysql -e "FLUSH BINARY LOGS DELETE_DOMAIN_ID=($domain)" >/dev/null 2>/dev/null

	if [ $? -eq 0 ]; then
		echo "Success! The domain is removed."
		exit 1
	fi
	echo "Failed, proceding to the next log"
	next_binary_log=$(mysql -Be "SHOW BINARY LOGS"|grep -v "Log_name"|head -n 2|tail -n 1|awk '{print $1}')
done

echo "ERROR! The process failed. Most likely this is because the domain you are trying to delete still receives updates."
